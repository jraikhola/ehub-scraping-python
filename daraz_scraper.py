#import Libraries
from selenium import webdriver  # for chrome open library for scraping
import time #time library for sleep
from selenium.webdriver.common.keys import Keys  # chrome keys libraries for input forms
from selenium.webdriver.common.action_chains import ActionChains #performing action libraries in chrome like Enter ESC
from bs4 import BeautifulSoup, NavigableString #Libraries to parse HTML  

category_name = 'womensshoes'

scraping_link = 'https://www.daraz.com.np/womens-shoes/'



#open chrome browser
driver = webdriver.Chrome(executable_path = r"/home/programmer-0/ehub-trading-ecom/data-scraping/chromedriver")

#list for all products URL save
list1=[]


#create CSV for Saving Data of mobile phones with sentiment polarity
data=open(category_name+'.csv','a',encoding="utf-8")

#writing header for the FILE
data.write("product_number,brand,product_name,price,image,product_detail,source_link,description,customer_options\n")



#First URL of DARAZ where mobile numbers list
URL = scraping_link
for i in range(1,2): #loop for total number of pages in DARAZ mobile phones
 
	URL = scraping_link+'?page='+str(i) # URL to open one by one
	driver.get(URL)  					#open that URL for finding the URLS of Every Product on that page
	print(URL) 				#printing the URL
	src=driver.page_source   # Getting The META DATA of that page
	soup=BeautifulSoup(src,'html.parser')  #parge that meta data in to HTML
	time.sleep(2)						#Sleep for 2 seconds
	urll=soup.findAll('div',{'data-qa-locator':'product-item'})  #finding the URLS of products
	for i in urll:
		i=i.find('a')   #finding the Tag a in that div
		print(i['href']) 
		list1.append(i['href'])    # now url is in the HREF
	


driver.quit() #Quit the first driver for Memory Cleaning

#list for reviews rating

urllist=list1 #now all the URLS i have in the list
countproduct=0 	#for priting the current product which is parsing at that time

driver = webdriver.Chrome(executable_path = r"/home/programmer-0/ehub-trading-ecom/data-scraping/chromedriver")


for i in urllist:			#parse one by one that product URL
	countproduct=countproduct+1		#increment the current product one by one
	print('Product '+str(countproduct))	#print the current product
	i=i.replace('\n','')  #removing the end line for open link clearly
	URL = 'https:'+str(i) 	#apped https: in the URL
	driver.get(URL)			
	print(URL)				
	src=driver.page_source	
	soup=BeautifulSoup(src,'html.parser') 
	time.sleep(2) 
	
	
	#pdp-mod-product-badge-title
	brandtitle=''			
	#checking product Name
	if(soup.find('span',{'class':'pdp-mod-product-badge-title'})):		#Checking the Brand Title Exist or not
		brandtitle=soup.find('span',{'class':'pdp-mod-product-badge-title'})	#Getting the Brand Title 
		brandtitle=brandtitle.get_text()			#Now Parse the HTML and Get the text in the html tag of that span 
		brandtitle=brandtitle.strip()				
		brandtitle=brandtitle.replace(',','')		
		brandtitle=brandtitle.replace('.','')
		brandtitle=brandtitle.replace('\n','')
		brandtitle=brandtitle.replace('\t','')
		brandtitle=brandtitle.replace('"','')
		print(brandtitle)


	description=''			
	#checking product Name
	if(soup.find('div',{'class':'pdp-product-detail'})):		#Checking the Brand Title Exist or not
		description=soup.find('div',{'class':'pdp-product-detail'})	#Getting the Brand Title 
		description=description.get_text()			#Now Parse the HTML and Get the text in the html tag of that span 
		description=description.strip()				
		description=description.replace(',','-')		
		description=description.replace('.','')
		description=description.replace('\n','')
		description=description.replace('\t','')
		description=description.replace('"','')
		print(description)



	customer_options_data=''			
	#checking product Name
	if(soup.find('div',{'class':'sku-selector'})):		#Checking the Brand Title Exist or not
		customer_options_data=soup.find('div',{'class':'sku-selector'})	#Getting the Brand Title 
		customer_options_data=customer_options_data.get_text()			#Now Parse the HTML and Get the text in the html tag of that span 
		customer_options_data=customer_options_data.strip()				
		customer_options_data=customer_options_data.replace(',','-')		
		customer_options_data=customer_options_data.replace('.','')
		customer_options_data=customer_options_data.replace('\n','')
		customer_options_data=customer_options_data.replace('\t','')
		customer_options_data=customer_options_data.replace('"','')
		print(customer_options_data)					

	
	
	brandname=''				 
	#checking Brand Name
	if(soup.find('div',{'class':'pdp-product-brand'})):			
		brandname=soup.find('div',{'class':'pdp-product-brand'})	
		brandname=brandname.find('a')					
		brandname=brandname.get_text()					
		brandname=brandname.strip()						
		print(brandname)								
	
	price=''							
	#checking price
	if(soup.find('span',{'class':'pdp-price pdp-price_type_normal pdp-price_color_orange pdp-price_size_xl'})):			
		price=soup.find('span',{'class':'pdp-price pdp-price_type_normal pdp-price_color_orange pdp-price_size_xl'})			
		price=price.get_text()			
		price=price.strip()	
		price = price.replace(',','')		
		print(price)
		
	image=''
	if(soup.find('img',{'class':'pdp-mod-common-image gallery-preview-panel__image'})):		
		image=soup.find('img',{'class':'pdp-mod-common-image gallery-preview-panel__image'})		
		image=image.get('src')
		image=image.strip()			
		print(image)

	product_detail=''
	if(soup.find('h2',{'pdp-mod-section-title outer-title'})):		
		product_detail=soup.find('h2',{'class':'pdp-mod-section-title outer-title'})		
		product_detail=product_detail.get_text()
		product_detail=product_detail.strip()
		product_detail = product_detail.replace(',','')			
		print(product_detail)

	data.write('Product '+str(countproduct)+","+brandname+","+brandtitle+","+price+","+image+"," +product_detail+"," +URL+","+description+","+customer_options_data+",") #writing in the CSV
	data.write('\n')
		
driver.quit()