import sys, os
from os.path import dirname, join, abspath
import configparser
import datetime
from bb_python_utils.libraries.database import MySqlDB
module_path = abspath(join(dirname(__file__), '../../'))
sys.path.insert(0, module_path)
import requests
import  json

dev_api ='http://54.151.205.27:3000/api/admin/v1/product-category/info'
local_api = 'http://127.0.0.1:3000/api/admin/v1/product-category/info'

class AutomateApi:
    def __init__(self):
        self.db = MySqlDB.MysqlDatabase(**{
            'mysql_pool_name': os.environ.get('pool_name'),
            'mysql_pool_size': int(os.environ.get('pool_size')),
            'mysql_host': os.environ.get('host'),
            'mysql_port': int(os.environ.get('port')),
            'mysql_user': os.environ.get('user'),
            'mysql_password': os.environ.get('password'),
            'database_name': os.environ.get('database_name')
        })


    def update_category_detail(self):
        try:
            url = dev_api
            token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJkOWZmMjlmN2Q4MGI0Nzg4YTFmZTkyMWFjOGE5NjQ0NCIsInJvbGVJZCI6MSwic2Vzc2lvbklkIjoiOTQ3NGYwNGNhNzJjNDM4MGJkN2I0ZTY3MzAwNzYwZWQiLCJpYXQiOjE2MTc4NzUxNzIsImV4cCI6MTYxNzk2MTU3MiwiYXVkIjoiZDlmZjI5ZjdkODBiNDc4OGExZmU5MjFhYzhhOTY0NDQiLCJpc3MiOiJlaHViIn0.JRPOf2cJkdpXoCOkdvlGRLVxMJWe1EMppIX2hUIr7W4'
            query = '''SELECT uuid from ehub_product_service.product_category'''
            cat_ids = self.db.handel_raw_qry(query)
            for category_data in cat_ids:
                cat_data = '''SELECT 
                            id,
                            uuid,
                            category_name,
                            meta_tag_title,
                            meta_tag_description,
                            meta_tag_keyword,
                            active,
                            slug
                        FROM
                            ehub_product_service.product_category
                        WHERE
                            deleted = 0
                                AND uuid = '%s' ''' % category_data.get('uuid')

                category = self.db.handel_raw_qry(cat_data)[0]

                brands = ''' SELECT 
                            brandTbl.uuid
                            FROM
                            ehub_product_service.brand as brandTbl
                            INNER JOIN
                            product_category_brand_map as mapTbl ON brandTbl.id = mapTbl.brand_id
                            WHERE
                            brandTbl.status = 1
                                AND brandTbl.deleted = 0
                                AND mapTbl.category_id = %s'''% category.get('id')

                brands_data = self.db.handel_raw_qry(brands)

                payload = {
                    "data" :{
                        "brandList":brands_data,
                        "categoryname" : category.get('category_name') or '',
                        "slug": category.get('slug') or '',
                        "metatagtitle": category.get('meta_tag_title') or '',
                        "metatagdescription": category.get('meta_tag_description') or '',
                        "metatagkeyword" : category.get('meta_tag_keyword') or '',
                        "status" : category.get('active') or '',
                        "uuid": category.get('uuid') or ''
                    }
                }

                headers = {
                    'Authorization': token

                }
               
                request_api = requests.put(url,json=payload,headers=headers)
                print(request_api)
                print(request_api.content)


        except Exception as e:
            print(e)


automate_fun = AutomateApi()
automate_fun.update_category_detail()