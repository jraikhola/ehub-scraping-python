import sys, os
from os.path import dirname, join, abspath
import configparser
import datetime
from bb_python_utils.libraries.database import MySqlDB
module_path = abspath(join(dirname(__file__), '../../'))
sys.path.insert(0, module_path)
import requests
import  json

dev_api ='http://54.151.205.27:3000/api/admin/v1/product-management/info'
base_api_url = 'http://127.0.0.1:3000/api/admin/v1/product-management/info'
stage_api = 'https://ehub-stage.bitsbeat.com/api/admin/v1/product-management/info'

class AutomateApi:
    def __init__(self):
        self.db = MySqlDB.MysqlDatabase(**{
            'mysql_pool_name': os.environ.get('pool_name'),
            'mysql_pool_size': int(os.environ.get('pool_size')),
            'mysql_host': os.environ.get('host'),
            'mysql_port': int(os.environ.get('port')),
            'mysql_user': os.environ.get('user'),
            'mysql_password': os.environ.get('password'),
            'database_name': os.environ.get('database_name')
        })


    def update_product_detail(self):
        try:
            url = dev_api
            token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJkOWZmMjlmN2Q4MGI0Nzg4YTFmZTkyMWFjOGE5NjQ0NCIsInJvbGVJZCI6MSwic2Vzc2lvbklkIjoiOTkwMGEyNWM0NmU4NDNiZDk4YmE5MjFmNjU0ZWI4ZDkiLCJpYXQiOjE2MjAwMjU5NTksImV4cCI6MTYyMDExMjM1OSwiYXVkIjoiZDlmZjI5ZjdkODBiNDc4OGExZmU5MjFhYzhhOTY0NDQiLCJpc3MiOiJlaHViIn0.mt9D0Urev6r-jbnZQJ8yXvrZXJ3hLuNMGjR8BJtUPA0'
            query = '''SELECT
                        basicinfo.uuid,
                        bnd.uuid as productbrand,
                        category.uuid as productcategory,
                        basicinfo.title,
                        basicinfo.short_description as shortdescription,
                        basicinfo.description,
                        images.path as img,
                        images.file_index as img_index,
                        basicinfo.active,
                        pricing.regular_price ,
                        pricing.sale_price,
                        pricing.shipping_cost,
                        inventory.sku,
                        inventory.track_quantity,
                        inventory.stock_quantity,
                        inventory.stock_availability,
                        attributes.title as a_title,
                        attributes.description as a_description,
                        customeroptions.title as c_title,
                        customeroptions.available_options as available_options,
                        seo.slug,
                        seo.alt_tag,
                        seo.meta_tag_title,
                        seo.meta_tag_description,
                        seo.meta_tag_keyword
                        FROM
                        ehub_product_service.product_info AS basicinfo
                            LEFT JOIN
                        ehub_product_service.product_pricing AS pricing ON basicinfo.id = pricing.product_id
                            LEFT JOIN
                        ehub_product_service.product_inventory AS inventory ON basicinfo.id = inventory.product_id
                            LEFT JOIN
                        ehub_product_service.product_attributes AS attributes ON basicinfo.id = attributes.product_id
                            LEFT JOIN
                        ehub_product_service.customer_options AS customeroptions ON basicinfo.id = customeroptions.product_id
                            LEFT JOIN
                        ehub_product_service.product_image as images On basicinfo.id = images.product_id
                            LEFT JOIN
                        ehub_product_service.product_category as category on basicinfo.product_category = category.id
                            LEFT JOIN
                        ehub_product_service.brand as bnd on basicinfo.product_brand = bnd.id
                            LEFT JOIN
                        ehub_product_service.product_seo AS seo ON basicinfo.id = seo.product_id'''


            data = self.db.handel_raw_qry(query)
            for products_data in data:
                payload =({
                    'uuid': products_data.get('uuid') or '',
                    'basicinfo' : {
                        'productbrand':str(products_data.get('productbrand')) or '',
                        'productcategory':str(products_data.get('productcategory')) or '',
                        'title':products_data.get('title'),
                        'shortdescription':products_data.get('shortdescription') or '',
                        'description':products_data.get('description') or '',
                        'imageList' : [
                            {
                                'path': products_data.get('img') or '',
                                'index': 1
                            }
                        ],
                        'status' : True,

                    },
                    'pricing': {
                        'regularprice': products_data.get('regular_price') or '',
                        'saleprice': products_data.get('sale_price') or '',
                        'shippingcost': products_data.get('shipping_cost') or ''

                    },
                    'inventory':{
                        'sku': products_data.get('sku') or '',
                        'trackquantity': products_data.get('track_quantity')or '',
                        'stockquantity': products_data.get('stock_quantity')or '',
                        'stockavailability': products_data.get('stock_availability')or ''
                    },
                    'attributes': [{
                        'title': products_data.get('a_title')or '',
                        'description': products_data.get('a_description')or ''

                    },],

                    'customeroptions':[{
                        'title': products_data.get('c_title')or '',
                        'options': products_data.get('available_options')or ''

                    }],
                    'seo':{
                        'slug': products_data.get('slug')or '',
                        'alttag': products_data.get('alt_tag')or '',
                        'metatagtitle': products_data.get('meta_tag_title')or '',
                        'metatagdescription': products_data.get('meta_tag_description')or '',
                        'metatagkeyword': products_data.get('meta_tag_keyword')or ''
                        
                    }
                })

                headers = {
                    'Authorization': token

                }
            
                request_api = requests.put(url,json=payload,headers=headers)
                print(request_api)
                print(request_api.content)


        except Exception as e:
            print(e)


automate_fun = AutomateApi()
automate_fun.update_product_detail()

