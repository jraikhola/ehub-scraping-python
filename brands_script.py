import sys, os
from os.path import dirname, join, abspath
import configparser
import datetime
from bb_python_utils.libraries.database import MySqlDB
module_path = abspath(join(dirname(__file__), '../../'))
sys.path.insert(0, module_path)
import requests
import  json

dev_api ='http://54.151.205.27:3000/api/admin/v1/brand-management/update_brand'
local_api = 'http://127.0.0.1:3000/api/admin/v1/brand-management/update_brand'

class AutomateApi:
    def __init__(self):
        self.db = MySqlDB.MysqlDatabase(**{
            'mysql_pool_name': os.environ.get('pool_name'),
            'mysql_pool_size': int(os.environ.get('pool_size')),
            'mysql_host': os.environ.get('host'),
            'mysql_port': int(os.environ.get('port')),
            'mysql_user': os.environ.get('user'),
            'mysql_password': os.environ.get('password'),
            'database_name': os.environ.get('database_name')
        })


    def update_category_detail(self):
        try:
            url = dev_api
            token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJkOWZmMjlmN2Q4MGI0Nzg4YTFmZTkyMWFjOGE5NjQ0NCIsInJvbGVJZCI6MSwic2Vzc2lvbklkIjoiOTQ3NGYwNGNhNzJjNDM4MGJkN2I0ZTY3MzAwNzYwZWQiLCJpYXQiOjE2MTc4NzUxNzIsImV4cCI6MTYxNzk2MTU3MiwiYXVkIjoiZDlmZjI5ZjdkODBiNDc4OGExZmU5MjFhYzhhOTY0NDQiLCJpc3MiOiJlaHViIn0.JRPOf2cJkdpXoCOkdvlGRLVxMJWe1EMppIX2hUIr7W4'
            query = '''SELECT uuid from ehub_product_service.brand'''
            ids = self.db.handel_raw_qry(query)
            for brands_data in ids:
                brands_query = "SELECT * FROM ehub_product_service.brand WHERE deleted = 0 AND uuid = '%s'"%brands_data.get('uuid')

                get_data = self.db.handel_raw_qry(brands_query)
                if not get_data:
                    continue
                
                brand_data = get_data[0]
                

                data ={}
                data['brandname'] = brand_data.get('brand_name') or ''
                data["image"] = brand_data.get('image') or ''
                data['alttag'] = brand_data.get('alt_tag') or ''
                data['metatagtitle'] = brand_data.get('meta_tag_title') or ''
                data['metatagdescription'] = brand_data.get('meta_tag_description') or ''
                data['metatagkeyword'] = brand_data.get('meta_tag_keyword') or ''
                data['status'] = brand_data.get('status') or ''
                data['uuid'] = brand_data.get('uuid') or ''
                data['slug'] = brand_data.get('slug') or ''

                new = {"data": data}

                headers = {
                    'Authorization': token

                }
                request_api = requests.put(url,json=new,headers=headers)
                print(request_api)
                print(request_api.content)
        except Exception as e:
            print(e)


automate_fun = AutomateApi()
automate_fun.update_category_detail()